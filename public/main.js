import { StackComponent } from "./modules/StackComponent.js";

let stack = new StackComponent(document.getElementById("stack-hook"), [
	"🍑",
	"🍎",
	"🥑",
	"🥥"
]);
stack.initView();
