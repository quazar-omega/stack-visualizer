import { Stack } from "./Stack.js";

export class StackComponent extends Stack {
	constructor(hook, initialValue = []) {
		super(initialValue);
		this.hook = hook;

		this.wrapperView = document.createElement("section");
		this.stackView = document.createElement("section");
		

		this.interactions = {
			push: {
				openDialog: document.createElement("button"),
				dialog: {
					form: document.createElement("form"),
					userInput: document.createElement("input"),
					submit: document.createElement("input")
				},
			},
			pop: document.createElement("button")
		};

		this.interactions.push.openDialog.innerText = "+";
		Object.assign(this.interactions.push.dialog.submit, {
			type: "submit",
			value: "✔ Push"
		});

		this.interactions.pop.innerText = "-";
		this.interactions.push.openDialog.addEventListener("click", () => {
			if (this.interactions.push.dialog.form.clientHeight) {
				this.interactions.push.dialog.form.style.height = 0;
				this.interactions.push.dialog.form.style.padding = 0;
			} else {
				this.interactions.push.dialog.form.style.height = this.interactions.push.dialog.form.scrollHeight + "px";
				this.interactions.push.dialog.form.style.padding = "5px";
			};
		});
		this.interactions.push.dialog.submit.addEventListener("click", (event) => {
			event.preventDefault();
			if (this.interactions.push.dialog.userInput.value != "") {
				this.push(this.interactions.push.dialog.userInput.value);
			}
		});
		
		this.interactions.pop.addEventListener("click", () => {
			this.pop();
		});


		this.interactions.push.dialog.form.append(
			this.interactions.push.dialog.userInput,
			this.interactions.push.dialog.submit
		);
		this.interactionsView = document.createElement("section");
		this.interactionsView.append(
			this.interactions.push.openDialog,
			this.interactions.push.dialog.form,
			this.interactions.pop
		);


		this.wrapperView.append(
			this.interactionsView,
			this.stackView
		);

		this.stackEmptyGraphic = document.createElement("section");
		this.stackEmptyGraphic.id = "empty";
		this.stackEmptyGraphic.innerHTML = `
			<svg 
			width="250" 
			height="250" 
			viewBox="0 0 66.145833 66.145833" 
			version="1.1" 
			fill="#000000" 
			xmlns="http://www.w3.org/2000/svg" 
			xmlns:svg="http://www.w3.org/2000/svg"> 
			<defs 
				id="defs8270" /> 
			<g 
				id="layer1"> 
				<g 
				id="g20244"> 
				<path 
					id="path8356" 
					d="m 38.304922,10.546005 a 23.2995,23.2995 0 0 1 17.613239,22.594981 v 0 A 23.2995,23.2995 0 0 1 32.618663,56.440484 23.2995,23.2995 0 0 1 9.3191621,33.140986 23.2995,23.2995 0 0 1 21.198149,12.832402" /> 
				<path 
					id="path8641-8" 
					d="m 10.969077,22.679257 c 0,0 3.413719,1.152533 6.709415,-0.337605 3.295695,-1.490139 4.549986,-4.265826 4.549986,-4.265826" /> 
				<path 
					id="path8641-5" 
					d="m 46.027738,13.905854 c 0,0 -2.649399,2.441823 -6.266264,2.421604 -3.616866,-0.02022 -5.892031,-2.045388 -5.892031,-2.045388" /> 
				<path 
					id="path8641-2" 
					d="m 54.126889,43.137993 c 0,0 -2.279245,-1.89987 -3.34398,-4.72614 -1.064736,-2.826271 0.524962,-5.347467 0.524962,-5.347467" /> 
				<path 
					id="path8641-2-8" 
					d="m 38.873172,43.356916 c 0,0 -2.81696,-0.229522 -4.861635,-2.452306 -2.044677,-2.222784 -2.250185,-4.766808 -2.250185,-4.766808" /> 
				<path 
					id="path8641-2-8-0" 
					d="m 21.358823,41.817139 c 0,0 -2.388394,-1.511131 -3.167358,-4.429124 -0.778966,-2.917993 0.220147,-5.266624 0.220147,-5.266624" /> 
				<path 
					id="path8641-2-8-6" 
					d="m 47.47369,37.072024 c 0,0 -2.550057,-1.21867 -3.667984,-4.024325 -1.117929,-2.805656 -0.403007,-5.255795 -0.403007,-5.255795" /> 
				<path 
					id="path8641-2-9" 
					d="m 37.072296,29.765492 c 0,0 2.185802,-1.791707 2.602298,-4.783027 0.416497,-2.991321 -0.862169,-5.200236 -0.862169,-5.200236" /> 
				<path 
					id="path8641-2-9-4" 
					d="m 21.567869,33.291959 c 0,0 2.824956,-0.08699 4.979261,-2.203692 2.154307,-2.116704 2.488012,-4.647106 2.488012,-4.647106" /> 
				<path 
					id="path8641-9" 
					d="m 46.917226,50.398068 c 0,0 3.301885,1.442002 6.71358,0.240978 3.411695,-1.201024 4.900175,-3.858488 4.900175,-3.858488" /> 
				<path 
					id="path8641-9-9" 
					d="m 51.073529,18.877332 c 0,0 1.418238,-3.312159 0.19273,-6.715137 C 50.040751,8.7592181 47.37266,7.2898741 47.37266,7.2898741" /> 
				<path 
					id="path8641-4" 
					d="m 21.334489,53.179517 c 0,0 -6.04022,-4.621952 -7.31877,-13.758667 -1.27855,-9.136716 3.01813,-12.776474 3.01813,-12.776474" /> 
				<path 
					id="path8641-4-6" 
					d="m 32.618663,56.440484 c 0,0 4.788621,-7.104203 6.067171,-16.240918 C 39.964384,31.06285 32.431213,18.98668 32.431213,18.98668" /> 
				<path 
					id="path8641-4-6-2" 
					d="m 30.113715,56.777121 c 0,0 -6.408207,-3.21926 -8.405789,-13.589477 -1.997582,-10.370218 4.36346,-18.739732 4.36346,-18.739732" /> 
				<path 
					id="path8641-4-6-2-6" 
					d="m 38.704908,55.833899 c 0,0 4.375114,-2.037754 8.138712,-13.891198 3.763597,-11.853445 -4.779586,-22.451427 -4.779586,-22.451427" /> 
				<path 
					id="path8641-0" 
					d="m 14.005761,33.77692 c 0,0 -1.417277,-1.417972 -1.542073,-3.487067 -0.124798,-2.069095 0.88283,-3.450653 0.88283,-3.450653" /> 
				<path
					id="path8641-0-4"
					
					d="m 46.024195,26.251751 c 0,0 1.124853,-1.659526 0.857721,-3.715096 -0.267129,-2.055571 -1.516927,-3.222628 -1.516927,-3.222628" /> 
				<path 
					id="path8641" 
					d="m 9.3191621,33.140986 c 0,0 -3.2767812,-1.498172 -4.6576598,-4.841119 C 3.2806236,24.95692 4.2676761,22.075355 4.2676761,22.075355" /> 
				<path 
					id="path8641-7" 
					d="m 13.625793,19.504138 c 0,0 -2.644503,-1.427179 -2.406617,-5.036269 0.237886,-3.60909 2.851527,-4.50583 2.851527,-4.50583" /> 
				</g> 
			</g> 
			</svg>
			<h2>Wow, the emptiness...</h2>
		`.replace(/[\t\n]/g, "");
	}

	/*
	 * @override
	 */
	push(element) {
		let pushed = super.push(element);
		this.updateView(StackAction.Push);
		return pushed;
	}
	/*
	 * @override
	 */
	pop() {
		this.updateView(StackAction.Pop);
		return super.pop();
	}

	initView() {

		this.hook.append(this.wrapperView);

		if (!this.isEmpty()) {

			this.stackView.innerHTML = `${
				this.elements.slice().reverse().map((element, i) => {
					return `
						<div class="element">
							<div class="index">${i}</div>
							<div class="value">${element}</div>
						</div>
					`;
				}).join("")}
			`.replace(/[\t\n]/g, "");
		} else {
			this.wrapperView.append(this.stackEmptyGraphic);
		}
	}

	updateView(action) {

/* 		function updateIndexes() {
			Array.from(this.stackView.children).forEach((element, i) => {
				element.firstChild.innerText = i;
			});
		} */

		if (action == StackAction.Push) {
			if (!this.isEmpty() && this.elements.length == 1) {
				this.wrapperView.removeChild(this.stackEmptyGraphic);
			}

			let pushed = document.createElement("div");
			pushed.className = "element";
			pushed.innerHTML = `
					<div class="index">0</div>
					<div class="value">${this.elements[this.elements.length - 1]}</div>
			`.replace(/[\t\n]/g, "");
			this.stackView.insertBefore(pushed, this.stackView.firstChild);
			Array.from(this.stackView.children).forEach((element, i) => {
				element.firstChild.innerText = i;
			});
		}

		if (action == StackAction.Pop && !this.isEmpty()) {
			
			this.stackView.firstChild.style.animation = "pop 1s";
			setTimeout(() => {
					this.stackView.removeChild(this.stackView.firstChild);
					Array.from(this.stackView.children).forEach((element, i) => {
						element.firstChild.innerText = i;
					});
				},
				1000
			);

			if (this.elements.length == 1) {
				this.wrapperView.append(this.stackEmptyGraphic);
			}
		}
	}
}

export const StackAction = {
	Push: "Push",
	Pop: "Pop"
};