export class Stack {
	constructor(elements = []) {
		this.elements = elements;
	}

	push(element) {
		this.elements.push(element)
		return this.elements[this.elements.length - 1];
	}

	pop() {
		return this.elements.pop();
	}

	isEmpty() {
		return this.elements.length == 0;
	}

	toString() {
		return this.elements.join();
	}
}